import { Component, OnInit, Input } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { AccountData, Doctor } from 'src/app/core/services/podaci';

@Component({
  selector: 'app-korisnik-tabela',
  templateUrl: './korisnik-tabela.component.html',
  styleUrls: ['./korisnik-tabela.component.css']
})
export class KorisnikTabelaComponent implements OnInit {
  @Input() tabelaKorisnika;
  pa;

  doctor: Doctor[] = [];

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.mainService.getDoctors().subscribe((data) => {
      this.doctor = data;
    })
  }

  delete(id: string) {
    // this.mainService.deleteUser(id).subscribe(() => {
    //   this.getAll();
    // });
  }

}
