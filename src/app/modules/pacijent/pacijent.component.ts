import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { Patient } from 'src/app/core/services/podaci';
import { Router, RouterStateSnapshot, RouterState, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pacijent',
  templateUrl: './pacijent.component.html',
  styleUrls: ['./pacijent.component.css']
})
export class PacijentComponent implements OnInit {
  pac;
  patient: any;

  constructor(
    private mainService: MainService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.mainService.getPatientById(id).subscribe((data: Patient) => {
      this.patient = data;
    });
  }

}
