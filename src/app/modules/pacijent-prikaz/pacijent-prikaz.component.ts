import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-pacijent-prikaz',
  templateUrl: './pacijent-prikaz.component.html',
  styleUrls: ['./pacijent-prikaz.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PacijentPrikazComponent implements OnInit {
  @Input() pacijent;

  constructor() { }

  ngOnInit() {
  }

}
