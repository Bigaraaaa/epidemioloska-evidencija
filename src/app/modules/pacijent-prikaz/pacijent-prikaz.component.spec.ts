import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacijentPrikazComponent } from './pacijent-prikaz.component';

describe('PacijentPrikazComponent', () => {
  let component: PacijentPrikazComponent;
  let fixture: ComponentFixture<PacijentPrikazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacijentPrikazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacijentPrikazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
