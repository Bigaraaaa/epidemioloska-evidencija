import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoviPacijentComponent } from './novi-pacijent.component';

describe('NoviPacijentComponent', () => {
  let component: NoviPacijentComponent;
  let fixture: ComponentFixture<NoviPacijentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoviPacijentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoviPacijentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
