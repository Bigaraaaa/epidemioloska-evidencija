import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnectionService } from 'src/app/core/services/connection.service';
import { MainService } from 'src/app/core/services/main.service';
import { Patient } from 'src/app/core/services/podaci';

@Component({
  selector: 'app-novi-pacijent',
  templateUrl: './novi-pacijent.component.html',
  styleUrls: ['./novi-pacijent.component.css']
})
export class NoviPacijentComponent implements OnInit {
  date = new Date();
  d = '03-03-2020';
  pacijent = {
    ime: '',
    prezime: '',
    broj: '',
    jmbg: '',
    lbo: '',
    adresa: '',
    ispostava: '',
    drzavljanstvo: '',
    uvoz: '',
    status: '',
    datum: this.date
  };

  patient: Patient = {
    clinicBranch: '',
    citizenship: '',
    countryOfImport: '',
    personalInfo: {
      name: '',
      surname: '',
      jmbg: '',
      lbo: '',
      phoneNumber: '',
      address: {
        city: '',
        street: '',
        streetNumber: ''
      }
    },
    contact: [{
      date: new Date(),
      description: '',
      name: '',
      surname: '',
      phoneNumber: ''
    }],
    mesure: [{
      mesure: '',
      startDate: new Date(),
      endDate: new Date(),
      institution: '',
      rescriptionNumber: ''
    }]
  };
  keyword = 'name';
  drzave = [];

  constructor(
    private router: Router,
    private connection: ConnectionService,
    private mainService: MainService
  ) { }

  ngOnInit() {
    this.drzave = this.connection.drzave;
    const c = JSON.parse(localStorage.getItem('contactInfo'));
    if (c != null) {
      this.patient.personalInfo.name = c.name;
      this.patient.personalInfo.surname = c.surname;
      this.patient.personalInfo.phoneNumber = c.phoneNumber;
      localStorage.removeItem('contactInfo');
    }
  }

  checked(value: string) {
    this.pacijent.status = value;
  }

  registracija() {
    console.log(this.patient);
    this.mainService.addPatient(this.patient).subscribe();
  }

  // autcomplete
  selectEvent(item, fieldName) {
    console.log({ item, fieldName });
    if (fieldName === 'countryOfImport') {
      this.patient.countryOfImport = item.name;
    } else {
      this.patient.citizenship = item.name;
    }
    console.log({ pac: this.patient });
  }

}
