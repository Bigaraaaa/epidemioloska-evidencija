import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacijentTableComponent } from './pacijent-table.component';

describe('PacijentTableComponent', () => {
  let component: PacijentTableComponent;
  let fixture: ComponentFixture<PacijentTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacijentTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacijentTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
