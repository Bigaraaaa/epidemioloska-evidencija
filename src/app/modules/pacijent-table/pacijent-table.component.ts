import { Component, OnInit } from '@angular/core';
import { Patient } from 'src/app/core/services/podaci';
import { MainService } from 'src/app/core/services/main.service';

@Component({
  selector: 'app-pacijent-table',
  templateUrl: './pacijent-table.component.html',
  styleUrls: ['./pacijent-table.component.css']
})
export class PacijentTableComponent implements OnInit {
  searchText;
  patient: Patient[] = [];
  pa = 1;

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.getAll();

  }

  getAll() {
    return this.mainService.getAllPateints().subscribe((data: Patient[]) => {
      this.patient = data;
    });
  }

  deletePatient(id: string) {
    return this.mainService.deletePatient(id).subscribe(() => {
      this.getAll();
    });
  }

  filterSearch(value: string) {
    this.searchText = value;
  }

}
