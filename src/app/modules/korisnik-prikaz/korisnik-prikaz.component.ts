import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { ActivatedRoute } from '@angular/router';
import { AccountData } from 'src/app/core/services/podaci';

@Component({
  selector: 'app-korisnik-prikaz',
  templateUrl: './korisnik-prikaz.component.html',
  styleUrls: ['./korisnik-prikaz.component.css']
})
export class KorisnikPrikazComponent implements OnInit {

  user: AccountData;
  id: string;

  

  constructor(
    private mainService: MainService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    // this.mainService.getUserById(this.id).subscribe((data: User) => {
    //   this.user = data;
    //   console.log(this.user);
    // });
  }

  // editUser() {
  //   console.log(this.user);
  //   this.mainService.editUserPin(this.user, this.id).subscribe();
  // }

}
