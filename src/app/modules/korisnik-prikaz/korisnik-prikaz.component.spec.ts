import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KorisnikPrikazComponent } from './korisnik-prikaz.component';

describe('KorisnikPrikazComponent', () => {
  let component: KorisnikPrikazComponent;
  let fixture: ComponentFixture<KorisnikPrikazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KorisnikPrikazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KorisnikPrikazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
