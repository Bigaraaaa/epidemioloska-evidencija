import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StanjeComponent } from './stanje.component';

describe('StanjeComponent', () => {
  let component: StanjeComponent;
  let fixture: ComponentFixture<StanjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StanjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
