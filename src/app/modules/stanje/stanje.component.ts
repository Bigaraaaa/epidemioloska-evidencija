import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-stanje',
  templateUrl: './stanje.component.html',
  styleUrls: ['./stanje.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StanjeComponent implements OnInit {
  @Input() status;

  @Input() mesure;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
  }

  novoStanje() {
    this.router.navigate(['/novostanje', this.route.snapshot.paramMap.get('id')]);
  }

}
