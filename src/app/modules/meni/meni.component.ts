import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/authorization/auth.service';

@Component({
  selector: 'app-meni',
  templateUrl: './meni.component.html',
  styleUrls: ['./meni.component.css']
})
export class MeniComponent implements OnInit {
  role;
  value = true;
  constructor(
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.role = this.auth.getCurrentRoles()[0];
  }

}
