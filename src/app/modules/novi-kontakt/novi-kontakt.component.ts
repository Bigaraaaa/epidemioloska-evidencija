import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-novi-kontakt',
  templateUrl: './novi-kontakt.component.html',
  styleUrls: ['./novi-kontakt.component.css']
})
export class NoviKontaktComponent implements OnInit {
  date = new Date();
  kontakt = {
    name: '',
    surname: '',
    description: '',
    date: this.date,
    phoneNumber: ''
  };

  constructor(
    private mainService: MainService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  dodaj() {
    console.log(this.kontakt);
    const id = this.route.snapshot.paramMap.get('id');
    this.mainService.addPatientContact(id, this.kontakt).subscribe(data => {
      this.router.navigate(['/pacijenti']);
    });
  }

}
