import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoviKontaktComponent } from './novi-kontakt.component';

describe('NoviKontaktComponent', () => {
  let component: NoviKontaktComponent;
  let fixture: ComponentFixture<NoviKontaktComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoviKontaktComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoviKontaktComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
