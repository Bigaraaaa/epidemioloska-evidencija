import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { AccountData, Doctor } from 'src/app/core/services/podaci';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.css']
})
export class RegistracijaComponent implements OnInit {
  doctor: Doctor = {
    accountData: {
      name: '',
      surname: '',
      phoneNumber: '',
      pin: '',
      permission: {
        authority: ''
      }
    }
    
   
  };

  constructor(private mainService: MainService) { }

  ngOnInit() {
  }

 

  registracija() {
    console.log(this.doctor);
    return this.mainService.addDoctor(this.doctor).subscribe();
  }

}
