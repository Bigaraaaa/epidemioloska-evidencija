import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MainService } from 'src/app/core/services/main.service';
import { Status } from 'src/app/core/services/podaci';

@Component({
  selector: 'app-novo-stanje',
  templateUrl: './novo-stanje.component.html',
  styleUrls: ['./novo-stanje.component.css']
})
export class NovoStanjeComponent implements OnInit {
  date = new Date();
  stanje: Status = {
    temperature: '',
    anamneses: '',
    description: '',
    date: this.date,
    status: ''
  };

  constructor(
    private router: Router,
    private mainService: MainService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  checked(value) {
    this.stanje.status = value;
  }

  dodaj() {
    console.log(this.stanje);
    this.mainService.addPatientStatus(this.route.snapshot.paramMap.get('id'), this.stanje).subscribe();
    this.router.navigate(['/pacijent']);
  }

  // autcomplete
  // selectEvent(item, fieldName) {
  //   console.log({ item, fieldName });
  //   if (fieldName === 'uvoz') {
  //     this.pacijent.uvoz = item.name;
  //   } else {
  //     this.pacijent.drzavljanstvo = item.name;
  //   }
  //   console.log({ pac: this.pacijent });
  // }

  // onChangeSearch(val: string) {
  //   // fetch remote data from here
  //   // And reassign the 'data' which is binded to 'data' property.
  // }

  // onFocused(e) {
  //   // do something when input is focused
  // }

}
