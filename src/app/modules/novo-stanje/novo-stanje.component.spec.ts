import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoStanjeComponent } from './novo-stanje.component';

describe('NovoStanjeComponent', () => {
  let component: NovoStanjeComponent;
  let fixture: ComponentFixture<NovoStanjeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoStanjeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoStanjeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
