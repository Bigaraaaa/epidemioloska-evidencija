import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-kontakt',
  templateUrl: './kontakt.component.html',
  styleUrls: ['./kontakt.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class KontaktComponent implements OnInit {
  @Input() contact;
  id;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    console.log(this.contact);
  }

  proglasiPacijentom(name, surname, phone) {
    // console.log('/novipacijent');
    this.router.navigate(['/novipacijent']);
    console.log(name, surname, phone);
    const contactInfo = {
      name, surname, phone
    };
    localStorage.setItem('contactInfo', JSON.stringify(contactInfo));
  }

  noviKontakt() {
    this.router.navigate(['/novikontakt', this.id]);
  }

}
