import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authorization/auth.service';
import { AccountData } from '../../services/podaci';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  accountData: AccountData = {
    name: '',
    surname: '',
    phoneNumber: '',
    pin: '',
    permission: {
      authority: ''
    }
  }
  

  constructor(private authService : AuthService) { }

  ngOnInit() {
  }

  onLogin(){
    this.authService.login(this.accountData.phoneNumber, this.accountData.pin);
    console.log(this.accountData.phoneNumber, this.accountData.pin);
  }
}
