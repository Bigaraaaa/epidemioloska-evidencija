import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Patient, Contact, Status, Doctor, Administrator, Student, Mup } from './podaci';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  private administratorURL = 'http://localhost:8080/administrator';
  private doctorURL = 'http://localhost:8080/doctor';
  private studentURL = 'http://localhost:8080/student';
  private mupURL = 'http://localhost:8080/mup';
  private patientURL = 'http://localhost:8080/patient';

  constructor(
    private httpClient: HttpClient
  ) {

  }

              // DoctorServices

  getDoctors() {
    return this.httpClient.get<Doctor[]>(this.doctorURL + `/all`);
  }

  getDoctorById(id: string) {
    return this.httpClient.get<Doctor>(this.doctorURL + `/${id}`);
  }

  addDoctor(doctor: Doctor) {
    return this.httpClient.post(this.doctorURL + `/add`, doctor);
  }

  editDoctor(id: string, doctor: Doctor) {
    return this.httpClient.put(this.doctorURL + `/${id}`, doctor);
  }

  deleteDoctor(id: string) {
    return this.httpClient.delete(this.doctorURL + `/${id}`);
  }

                // AdministratorService

  getAdministrators() {
    return this.httpClient.get<Administrator[]>(this.administratorURL + `/all`);
  }

  getAdministratorById(id: string) {
    return this.httpClient.get<Administrator>(this.administratorURL + `/${id}`);
  }

  addAdministrator(administrator: Administrator) {
    return this.httpClient.post(this.administratorURL + `/add`, administrator);
  }

  editAdministrator(id: string, administrator: Administrator) {
    return this.httpClient.put(this.administratorURL + `/${id}`, administrator);
  }

  deleteAdministrator(id: string) {
    return this.httpClient.delete(this.administratorURL + `/${id}`);
  }

                      // StudentServices
  
  getStudents() {
    return this.httpClient.get<Student[]>(this.studentURL + `/all`);
  }

  getStudentById(id: string) {
    return this.httpClient.get<Student>(this.studentURL + `/${id}`);
  }

  addStudent(student: Student) {
    return this.httpClient.post(this.studentURL + `/add`, student);
  }

  editStudent(id: string, student: Student) {
    return this.httpClient.put(this.studentURL + `/${id}`, student);
  }

  deleteStudent(id: string) {
    return this.httpClient.delete(this.studentURL + `/${id}`);
  }

                  // MupServices

  getMup() {
    return this.httpClient.get<Mup[]>(this.mupURL + `/all`);
  }

  getMupById(id: string) {
    return this.httpClient.get<Mup>(this.mupURL + `/${id}`);
  }

  addMup(mup: Mup) {
    return this.httpClient.post(this.mupURL + `/add`, mup);
  }

  editMup(id: string, mup: Mup) {
    return this.httpClient.put(this.mupURL + `/${id}`, mup);
  }

  deleteMup(id: string) {
    return this.httpClient.delete(this.mupURL + `/${id}`);
  }


  getAllPateints() {
    return this.httpClient.get<Patient[]>(this.patientURL + `/all`);
  }

  getPatientById(id: string) {
    return this.httpClient.get<Patient>(this.patientURL + `/${id}`);
  }

  // getUserById(id: string) {
  //   return this.httpClient.get<User>(this.usersURL +`/${id}`);
  // }

  addPatient(patient: Patient) {
    console.log(patient);
    return this.httpClient.post(this.patientURL + `/add`, patient);
  }

  addPatientContact(id: string, contact: Contact) {
    console.log(id);
    return this.httpClient.put(this.patientURL + `/add/${id}`, contact);
  }

  addPatientStatus(id: string, status: Status) {
    console.log(id);
    return this.httpClient.put(this.patientURL + `/addstatus/${id}`, status);
  }

  editPatient(id: string, patient: Patient) {
    return this.httpClient.put(this.patientURL + `/${id}`, patient);
  }

  // editUserPin(user: User, id: string) {
  //   return this.httpClient.put(this.usersURL + `/password/${id}`, user);
  // }

  deletePatient(id: string) {
    return this.httpClient.delete(this.patientURL + `/${id}`);
  }

  // deleteUser(id: string) {
  //   return this.httpClient.delete(this.usersURL + `/${id}`);
  // }
}
