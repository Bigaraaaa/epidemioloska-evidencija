let pacijent = {
    ime: 'Marko',
    prezime: 'Markovic',
    broj: '0637365645',
    jmbg: '1205954857765',
    lbo: '1234621',
    adresa: 'Nadezde Petrovic 7',
    ispostava: 'NN',
    drzavljanstvo: 'Srbija',
    uvoz: 'Srbija',
    status: 'Pacijent',
    datum: new Date()
};

export interface Doctor {
    accountData: AccountData;
}

export interface Administrator {
    accountData: AccountData;
}

export interface Student {
    accountData: AccountData;
}

export interface Mup {
    accountData: AccountData;
}

export interface AccountData {
    phoneNumber: string;
    pin: string;
    name: string;
    surname: string;
    permission: Permission;
}

export interface Permission {
    authority: string;
}

export interface Patient {
    clinicBranch: string,
    citizenship: string;
    countryOfImport: string;
    personalInfo: PersonalInfo;
    contact: [Contact]
    mesure: [Mesure]
}

export interface Contact {
    date: Date;
    description: string;
    name: string;
    surname: string;
    phoneNumber: string;
}

export interface Status {
    date: Date;
    temperature: string;
    description: string;
    status: string;
    anamneses: string;
}

export interface Mesure {
    mesure: string;
    startDate: Date;
    endDate: Date;
    institution: string;
    rescriptionNumber: string;
}

export interface PersonalInfo {
    name: string;
    surname: string;
    jmbg: string;
    lbo: string;
    phoneNumber: any;
    address: Address;
}

export interface Address {
    city: string;
    street: string;
    streetNumber: string;
}

