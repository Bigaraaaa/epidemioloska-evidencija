import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './core/auth/login/login.component';
import { PacijentTableComponent } from './modules/pacijent-table/pacijent-table.component';
import { RegistracijaComponent } from './modules/registracija/registracija.component';
import { KontaktComponent } from './modules/kontakt/kontakt.component';
import { StanjeComponent } from './modules/stanje/stanje.component';
import { MeniComponent } from './modules/meni/meni.component';
import { NoviPacijentComponent } from './modules/novi-pacijent/novi-pacijent.component';
import { PacijentComponent } from './modules/pacijent/pacijent.component';
import { NovoStanjeComponent } from './modules/novo-stanje/novo-stanje.component';
import { RoleGuard } from './core/auth/authorization/role.guard';
import { NoviKontaktComponent } from './modules/novi-kontakt/novi-kontakt.component';
import { KorisnikTabelaComponent } from './modules/korisnik-tabela/korisnik-tabela.component';
import { KorisnikPrikazComponent } from './modules/korisnik-prikaz/korisnik-prikaz.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'pacijenti', component: PacijentTableComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'pacijent/:id', component: PacijentComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'novipacijent', component: NoviPacijentComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'novikontakt/:id', component: NoviKontaktComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'novostanje/:id', component: NovoStanjeComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'kontakt', component: KontaktComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'stanje', component: StanjeComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'registracija', component: RegistracijaComponent,
    // canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_ADMINISTRATOR'] }
  },
  {
    path: 'home', component: MeniComponent,
    canActivate: [RoleGuard], data: { expectedRoles: ['ROLE_DOCTOR', 'ROLE_STUDENT', 'ROLE_MUP', 'ROLE_ADMINISTRATOR'] }
  },
  // {
  //   path: 'tabelakorisnika', component: KorisnikTabelaComponent,
  // },
  {
    path: 'korisnik/:id', component: KorisnikPrikazComponent
  },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
