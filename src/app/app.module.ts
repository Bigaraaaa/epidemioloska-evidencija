import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/auth/login/login.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './core/auth/authorization/auth-interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PacijentTableComponent } from './modules/pacijent-table/pacijent-table.component';
import { KontaktComponent } from './modules/kontakt/kontakt.component';
import { RegistracijaComponent } from './modules/registracija/registracija.component';
import { StanjeComponent } from './modules/stanje/stanje.component';
import { MeniComponent } from './modules/meni/meni.component';
import { NoviPacijentComponent } from './modules/novi-pacijent/novi-pacijent.component';
import { PacijentComponent } from './modules/pacijent/pacijent.component';
import { PacijentPrikazComponent } from './modules/pacijent-prikaz/pacijent-prikaz.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NovoStanjeComponent } from './modules/novo-stanje/novo-stanje.component';
import { FilterSearchPipe } from './core/pipes/filter-search.pipe';
import { NoviKontaktComponent } from './modules/novi-kontakt/novi-kontakt.component';
import { KorisnikTabelaComponent } from './modules/korisnik-tabela/korisnik-tabela.component';
import { KorisnikPrikazComponent } from './modules/korisnik-prikaz/korisnik-prikaz.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PacijentTableComponent,
    KontaktComponent,
    RegistracijaComponent,
    StanjeComponent,
    MeniComponent,
    NoviPacijentComponent,
    PacijentComponent,
    PacijentPrikazComponent,
    NovoStanjeComponent,
    FilterSearchPipe,
    NoviKontaktComponent,
    KorisnikTabelaComponent,
    KorisnikPrikazComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AutocompleteLibModule,
    NgxPaginationModule
  ],
  providers: [FilterSearchPipe, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
